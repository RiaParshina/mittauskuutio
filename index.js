const electron = require('electron')
var electronapp = electron.app
var BrowserWindow = electron.BrowserWindow


// vendor libraries
var express = require('express');
var bodyParser = require('body-parser');
var cookieParser = require('cookie-parser');
var session = require('express-session');
var bcrypt = require('bcrypt-nodejs');
var ejs = require('ejs');
var path = require('path');
var passport = require('passport');
var LocalStrategy = require('passport-local').Strategy;

// custom libraries
// routes
var route = require('./route');
// model
var Model = require('./db');

var app = express();

passport.serializeUser(function(user, done) {
  done(null, user.username);
});

passport.deserializeUser(function(username, done) {
   new Model.User({username: username}).fetch().then(function(user) {
      done(null, user);
   });
});

passport.use(new LocalStrategy(function(username, password, done) {
   new Model.User({username: username}).fetch().then(function(data) {
      var user = data;
      if(user === null) {
         return done(null, false, {message: 'Invalid user'});
      } else {
         user = data.toJSON();
         if(!bcrypt.compareSync(password, user.password)) {
            return done(null, false, {message: 'Invalid password'});
         } else {
            return done(null, user);
         }
      }
   });
}));

app.set('port', process.env.PORT || 3000);
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'ejs');

app.use(cookieParser());
app.use(bodyParser());
app.use(session({secret: 'secret strategic xxzzz code'}));
app.use(passport.initialize());
app.use(passport.session());

app.use(express.static(__dirname + '/bower_components')); // bootstrap
app.use(express.static(__dirname + '/public'));

// GET
app.get('/', route.index);

// signin
// GET
app.get('/signin', route.signIn);
// POST
app.post('/signin', route.signInPost);

app.get('/home', route.home);
// signup
// GET
app.get('/signup', route.signUp);
// POST
app.post('/signup', route.signUpPost);
// post request for our AJAX
app.post('/users', route.fetchUser);
// logout
// GET
app.get('/signout', route.signOut);

// Select devices view
app.get('/selectDevices', route.selectDevices);

/********************************/

/********************************/
// 404 not found
app.use(route.notFound404);

var server = app.listen(app.get('port'), function(err) {
   if(err) throw err;

   var message = 'Server is running @ http://localhost:' + server.address().port;
   console.log(message);
});




electronapp.on('ready', function(){
  var mainWindow = new BrowserWindow({
    width: 800,
    height: 600
  })
  mainWindow.loadURL('http://127.0.0.1:3000/')
  // open developer tools
  mainWindow.webContents.openDevTools()

  //method created for export to apps.js
  // exports.openWindow = () => {
  //   // var win = new BrowserWindow({width:400, height:200})
  //   mainWindow.loadURL('file://' + __dirname + '/views/login.ejs')
  // }
})
