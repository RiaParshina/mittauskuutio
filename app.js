const remote = require('electron').remote  // import remote from lib
const index = remote.require('./index.js')  //imports index.js methods which were exported (openWindow)


// creates button
var button = document.createElement('button')
button.textContent = 'Log in'
button.addEventListener('click', () => {
  index.openWindow()
}, false)
document.body.appendChild(button)


/* ----------------
  MySQL connection:
-------------------*/

var mysql = require("mysql");

var con = mysql.createConnection({
  host: "localhost",
  user: "root",
  password: "",
  database: "cubeUsers"
});

con.connect(function(err){
  if(err){
    console.log('Error connecting to Db');
    return;
  }
  console.log('Connection established');
});

con.query('SELECT * FROM users',function(err,rows){
  if(err) throw err;

  console.log('Data received from Db:\n');
  console.log(rows);
  for (var i = 0; i < rows.length; i++) {
    console.log(rows[i].Name);
  };

  var greeting = document.createElement('h2');
  var personalData = document.createElement('p');
  personalData.textContent = 'Your last weight result: ' + rows[2].Weight + 'kg'
  greeting.textContent = 'Hello ' + rows[2].Name + '!';
  document.body.appendChild(greeting)
  document.body.appendChild(personalData)
});
