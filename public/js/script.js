
$('label.btn').on('change', changeColor);
$('button.selectAll').on('click', deselectAll);
//$('.measuringDevices').on('click', enableNextButton);

function enableNextButton() {
	var deviceButtons = document.getElementsByClassName("measuringDevices");
	var t = $(deviceButtons[1]).attr('class');

	for (i=0; i < deviceButtons.length; i++) {
		if ($(deviceButtons[i]).attr('class') === "active") {
			$('.absolute').toggle();
			return;
		};
	};
};

function changeColor() {
	var counter = 0;
	var deviceButtons = document.getElementsByClassName("measuringDevices");

	 if (this.style.backgroundColor === 'rgb(255, 210, 0)' || this.style.backgroundColor === "") {
		$(this).css('background-color', '#bda019');
		for (i=0; i < deviceButtons.length; i++) {
			if (deviceButtons[i].style.backgroundColor === 'rgb(189, 160, 25)') {
				counter++;
			}
		};
		if (counter > 0) {
			$('.absolute').removeClass('disabled');
		};
	 }
	else {
		$(this).css('background-color', '#ffd200');
		$(this).removeClass('active');
		for (i=0; i < deviceButtons.length; i++) {
			if (deviceButtons[i].style.backgroundColor === 'rgb(189, 160, 25)') {
				counter++;
			}
		};
	}
  // user must choose Weight and Height when choosing Body Composition:
  if (deviceButtons[2].style.backgroundColor === 'rgb(189, 160, 25)') {
    if (deviceButtons[2].style.backgroundColor !== deviceButtons[0].style.backgroundColor || deviceButtons[2].style.backgroundColor !== deviceButtons[1].style.backgroundColor) {
      $('.absolute').addClass('disabled');
      document.getElementById("huom").innerHTML = "<span>HUOM!</span> Tehdäksesi kehonkoostumusmittaukset, sinun on myös valittava pituuden ja painon mittaus.";
    } else {
      document.getElementById("huom").innerHTML = "";
    }
  } else {
     document.getElementById("huom").innerHTML = "";
     $('.absolute').removeClass('disabled');
  }
  if (counter === 0) {
    $('.absolute').addClass('disabled');
  };
};

// toggle button to select / deselect all options
function deselectAll() {

  var tbutton = document.getElementById("toggle-btn");
  var listOfCheckboxes = document.getElementsByClassName("checkbox");

  if (tbutton.innerHTML == "Valitse kaikki" ) {
    tbutton.innerHTML = "Poista kaikki";
    for (i=0; i < listOfCheckboxes.length; i++) {
  		listOfCheckboxes[i].checked = true;
  	};
    $('label.btn').addClass('active');
  	$('label.btn').css('background-color', '#bda019');
  	$('.absolute').removeClass('disabled');
  }
  else {
    tbutton.innerHTML = "Valitse kaikki";
    for (i=0; i < listOfCheckboxes.length; i++) {
  		listOfCheckboxes[i].checked = false;
  	};
    $('label.btn').removeClass('active');
  	$('label.btn').css('background-color', '#ffd200');
  	$('.absolute').addClass('disabled');
  }
}
