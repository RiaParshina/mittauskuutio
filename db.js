// Connecting to db through Knex library, Knex instance created:
var knex = require('knex')({
  client: 'mysql',
  connection: {
    host: 'localhost',
    user: 'root',
    password: '',
    database: 'cubeUsers'
  }
});

// Bookshelf instance to create models
var bookshelf = require('bookshelf')(knex);

// User model created by Bookshelf, added 2 fields
var User = bookshelf.Model.extend({
   tableName: 'Users',
   idAttribute: 'id',
});

module.exports = {
   User: User
};
